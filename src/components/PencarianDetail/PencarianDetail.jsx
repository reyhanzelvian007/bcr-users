import style from './PencarianDetail.module.css';

const PencarianDetail = () => {
  return (
    <>
        <div className={style.cariMobilContainer}>
                <div>
                    <h6>Pencarianmu</h6>
                </div>
                <div className={style.cariMobil}>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Tipe Driver</p>
                        </div>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                Pilih Tipe Driver
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Tanggal</p>
                        </div>
                        <div></div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Waktu Jemput/Ambil</p>
                        </div>
                        <div></div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Jumlah Penumpang (optional)</p>
                        </div>
                        <div></div>
                    </div>
                </div>
            </div>
    </>
  )
}

export default PencarianDetail;