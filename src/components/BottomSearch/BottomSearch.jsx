import Pencarian from "../Pencarian/Pencarian";
import Footer from "../Footer/Footer";
import style from './BottomSearch.module.css';
import CardContainer from "../CardContainer/CardContainer";

const BottomSearch = () => {
    return (
        <>
          <div className={style.bawahContainer}>
              <Pencarian />
              <CardContainer />
              <Footer />
          </div>
        </>
    )
  }
  
export default BottomSearch;