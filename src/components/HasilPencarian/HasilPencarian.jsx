import React from 'react';
import BottomSearch from '../BottomSearch/BottomSearch';
import Top from '../Top/Top';

const HasilPencarian = () => {
  return (
    <>
        <Top />
        <BottomSearch />
    </>
  )
}

export default HasilPencarian;