import style from './DetailPaket.module.css';
import image from '../../images/fi_chevron-up.svg';

const DetailPaket = () => {
  return (
    <>
        <div className={style.container}>
            <div className={style.detailPaketBox}>
                <div className={style.detailPaketContainer}>
                    <div>
                        <div className={style.tentangPaket}>
                            <h6>Tentang Paket</h6>
                            <p>Include</p>
                            <ul>
                                <li>Apa saja yang termasuk dalam paket misal durasi max 12 jam</li>
                                <li>Sudah termasuk bensin selama 12 jam</li>
                                <li>Sudah termasuk Tiket Wisata</li>
                                <li>Sudah termasuk pajak</li>
                            </ul>
                            <p>Exclude</p>
                            <ul>
                                <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                <li>Tidak termasuk akomodasi penginapan</li>
                            </ul>
                        </div>
                        <div className={style.refund}>
                            <div className={style.refundContainer}>
                                <div>
                                    <h6>Refund, Reschedule, Overtime</h6>
                                </div>
                                <div>
                                    <img src={image} alt="chevron-up" />
                                </div>
                            </div>
                            <ul>
                                <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                <li>Tidak termasuk akomodasi penginapan</li>
                                <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                <li>Tidak termasuk akomodasi penginapan</li>
                                <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                <li>Tidak termasuk akomodasi penginapan</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <button>Lanjutkan Pembayaran</button>
            </div>
        </div>
    </>
  )
}

export default DetailPaket;