import React from 'react';
import style from '../Footer/Footer.module.css';
import icon_fb from '../../images/icon_facebook.svg';
import icon_ig from '../../images/icon_instagram.svg';
import icon_mail from '../../images/icon_mail.svg';
import icon_twitch from '../../images/icon_twitch.svg';
import icon_twitter from '../../images/icon_twitter.svg';

const Footer = () => {
  return (
    <>
        <div className={style.footerContainer}>
            <div className={style.address}>
                <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                <p>binarcarrental@gmail.com</p>
                <p>081-233-334-808</p>
            </div>
            <div className={style.navigation}>
                <a href="#" className={style.navItem}>Our services</a>
                <a href="#" className={style.navItem}>Why Us</a>
                <a href="#" className={style.navItem}>Testimonial</a>
                <a href="#" className={style.navItem}>FAQ</a>
            </div>
            <div className={style.sosmed}>
                <p>Connect with us</p>
                <div className={style.sosmedIcon}>
                    <img src={icon_fb} alt="icon_fb" />
                    <img src={icon_ig} alt="icon_ig" />
                    <img src={icon_mail} alt="icon_mail" />
                    <img src={icon_twitch} alt="icon_twitch" />
                    <img src={icon_twitter} alt="icon_twitter" />
                </div>
            </div>
            <div className={style.copyright}>
                <p>Copyright Binar 2022</p>
                <div className={style.logo}></div>
            </div>
        </div>
    </>
  )
}

export default Footer;