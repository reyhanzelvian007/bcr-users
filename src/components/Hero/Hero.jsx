import React from "react";
import style from './Hero.module.css'
import Navbar from "../Navbar/Navbar";
import image from "../../images/img_car.png"

const Hero = () => {
    return (
        <div className={style.heroContainer}>
            <div className="container">
                <Navbar />
                <div className={style.Hero}>
                    <div>
                        <h1>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                        <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                    </div>
                    <div>
                        <img src={image} alt="car-image" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Hero;