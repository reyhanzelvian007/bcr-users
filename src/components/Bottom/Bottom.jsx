import React from 'react';
import style from '../Bottom/Bottom.module.css'
import CariMobil from '../CariMobil/CariMobil';
import Footer from '../Footer/Footer';

const Bottom = () => {
  return (
      <>
        <div className={style.bottomContainer}>
            <CariMobil />
            <Footer />
        </div>
      </>
  )
}

export default Bottom;