import usersImage from '../../images/fi_users.svg';
import settingImage from '../../images/fi_settings.svg';
import calendarImage from '../../images/fi_calendar.svg';
import style from './Card.module.css';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

const Card = () => {

    const { dataCar } = useSelector((state) => state.carDataReducer);

    return (
        <>
            {dataCar.map((data) => (
                <div className={style.cardContainer}>
                    <div className={style.cardInside}>
                        <div className={style.cardImage}>
                            <img src={data.image} alt="car-image" />
                        </div>
                        <div className={style.cardDesc}>
                            <h6>{data.name}</h6>
                            <h5>Rp {data.price} / hari</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                        <div className={style.cardPoint}>
                            <img src={usersImage} alt="users-image" />
                            <p>4 orang</p>
                        </div>
                        <div className={style.cardPoint}>
                            <img src={settingImage} alt="setting-image" />
                            <p>Manual</p>
                        </div>
                        <div className={style.cardPoint}>
                            <img src={calendarImage} alt="calendar-image" />
                            <p>Tahun 2020</p>
                        </div>
                    </div>
                    <div className={style.buttonContainer}>
                        <Link to={`/car-detail/${data.id}`}>
                            <button>Pilih Mobil</button>
                        </Link>
                    </div>
                </div>
            ))}
        </>
  )
}


export default Card;