import Footer from "../Footer/Footer";
import style from './BottomDetail.module.css';
import Pencarian from '../Pencarian/Pencarian';
import DetailPaket from "../DetailPaket/DetailPaket";
import CardDetail from "../CardDetail/CardDetail";
import { useDispatch } from "react-redux";
import { dataCarAction } from "../../redux/actions/dataCarAction";

const BottomDetail = () => {

  const dispatch = useDispatch();

  dispatch(dataCarAction());

    return (
        <>
          <div className={style.bawahContainer}>
              <Pencarian />
              <div className={style.containerDetailPaket}>
                <DetailPaket />
                <CardDetail />
              </div>
              <Footer />
          </div>
        </>
    )
  }
  
export default BottomDetail;