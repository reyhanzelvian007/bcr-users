import React from "react";
import style from './Pencarian.module.css';

const Pencarian = () => {
    return (
        <>
            <div className={style.cariMobilContainer}>
                <div>
                    <h6>Pencarianmu</h6>
                </div>
                <div className={style.cariMobil}>
                <div className={style.poinCariMobil}>
                        <div>
                            <p>Tipe Driver</p>
                        </div>
                        <div>
                            <select name="tipeDriver" id="tipeDriver" placeholder="Supir">
                                <option value="sopir">Dengan Sopir</option>
                                <option value="tanpa-sopir">Tanpa Sopir (Lepas Kunci)</option>
                                <option disabled value="default">Pilih Tipe Driver</option>
                            </select>
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Tanggal</p>
                        </div>
                        <div>
                            <input id="date" type="date" value/>
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Waktu Jemput/Ambil</p>
                        </div>
                        <div>
                            <input type="time" />
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Jumlah Penumpang (optional)</p>
                        </div>
                        <div>
                            <input placeholder="Jumlah Penumpang (optional)" type="number" />
                        </div>
                    </div>
                    <div>
                        <button className={style.buttonCariMobil}>Edit</button>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Pencarian;