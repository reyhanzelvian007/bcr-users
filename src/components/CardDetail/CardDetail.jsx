import style from './CardDetail.module.css';
import car_image from '../../images/car_detail.png';
import user_image from '../../images/fi_users_detail.svg';
import setting_image from '../../images/fi_settings_detail.svg';
import calendar_image from '../../images/fi_calendar_detail.svg';
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';

const CardDetail = () => {

    const { idCar } = useParams();

    const { dataCar } = useSelector((state) => state.carDataReducer);

    let index = -1;

    for (let i=0; i < dataCar.length; i++){
        if (dataCar[i].id == idCar) {
            index = i;
        }
    }

  return (
    <>
        <div className={style.container}>
            <div className={style.imgContainer}>
                <img src={dataCar[index].image} alt="car-detail" />
            </div>
            <div className={style.cardDesc}>
                <div>
                    <h6>{dataCar[index].name}</h6>
                </div>
                <div className={style.cardPointContainer}>
                    <div className={style.cardPoint}>
                        <img src={user_image} alt="user-image" />
                        <p>4 orang</p>
                    </div>
                    <div className={style.cardPoint}>
                        <img src={setting_image} alt="setting-image" />
                        <p>Manual</p>
                    </div>
                    <div className={style.cardPoint}>
                        <img src={calendar_image} alt="calendar-image" />
                        <p>Tahun 2020</p>
                    </div>
                </div>
            </div>
            <div className={style.total}>
                <div>
                    <p>Total</p>
                </div>
                <div>
                    <h6>Rp {dataCar[index].price}</h6>
                </div>
            </div>
            <div>
                <button>Lanjutkan Pembayaran</button>
            </div>
        </div>
    </>
  )
}

export default CardDetail;