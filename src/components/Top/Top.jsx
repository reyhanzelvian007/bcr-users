import React from 'react';
import Navbar from '../Navbar/Navbar';
import style from './Top.module.css';

const Top = () => {
  return (
    <>
        <div className={style.topContainer}>
            <Navbar />
        </div>
    </>
  )
}

export default Top;