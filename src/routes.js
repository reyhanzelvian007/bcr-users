import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import App from './App';
import HasilPencarian from './components/HasilPencarian/HasilPencarian';
import DetailMobil from './components/DetailMobil/DetailMobil';
import NotFound from './components/NotFound/NotFound';

const RouteApp = () => {
    return (
        <>
            <Router>
                <Routes>
                    <Route path='/' element={<App />} />
                    <Route path='/search-result' element={<HasilPencarian />} />
                    <Route path='/car-detail/:idCar' element={<DetailMobil />} />
                    <Route path='*' element={<NotFound />} />
                </Routes>
            </Router>
        </>
    )
}

export default RouteApp;