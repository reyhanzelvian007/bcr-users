import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const globalStore = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunk))
)

console.log(globalStore.getState());

export default globalStore;